import java.util.*;
public class Hangman{
	
	public static void main (String[] args){
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Please choose a four lettered word.");
		
		String word = reader.nextLine();
	
		runGame(word);
	}
	
	public static int isLetterInWord(String word, char c){
		for(int i = 0; i < word.length(); i++ ){
			if(word.charAt(i) == (c)){			//Checks if the letter is in the word
				return i;						// Returns the position of the letter found
			}
		}		
		return -1;
	}
	
	public static char toUpperCase(char c){
		return Character.toUpperCase(c);		// returns a char to upperCase
	
	}
	
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String s = ""; 
		boolean [] boolArray = new boolean [4]; // created an array of 		booleans to match with the letters
		boolArray [0] = letter0;
		boolArray [1] = letter1;
		boolArray [2] = letter2;
		boolArray [3] = letter3;
		
		for(int i = 0; i < 4; i++){				// this will go through each value in the array
			if(boolArray[i] == true){
				s += toUpperCase(word.charAt(i));
			}
			else{
				s += "_";
			}
		}
		
		System.out.println("Your results are " + s);
	}
	
	public static void runGame(String word){
		//main loop of the game
		
		int missesCount = 0;
		
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		
		boolean [] boolArray = new boolean[4];
		boolArray [0]= letter0;
		boolArray [1]= letter1;		// created a new array
		boolArray [2]= letter2;
		boolArray [3]= letter3;
		
		
		
		while(missesCount < 6){
			Scanner userChar = new Scanner(System.in);  
			System.out.println("Please guess a letter.");

			char userCharGuess = userChar.nextLine().charAt(0);  
			int indentOfLetter = isLetterInWord(word, userCharGuess);
			
			for(int i = 0; i < boolArray.length; i++){
				if(indentOfLetter == i){	// this checks if the indentOfLetter matches with the index of the new array
					boolArray[i] = true; // if it matches, switch it to true
					break;
				}
			}
			if(indentOfLetter == -1){	// if not, one less chance
				missesCount += 1;
				System.out.println("You have " + missesCount +  " less guesses out of six.");
			}
				
			printWork(word, boolArray[0], boolArray[1], boolArray[2], boolArray[3]);
			
			boolean allLettersGuessed = true;
			for (int i = 0; i < boolArray.length; i++){
				if(!boolArray[i]){	// if the boolean value at the index is false, there isn't a win							
					allLettersGuessed = false;
					break;
				}
			}	
			if(allLettersGuessed){	// if this is true, that means you won
				System.out.println("You Won!");
				break;
			}
			if(missesCount == 6){	// if missesCount are 6, break the game, you lost
				System.out.println("You lost!");
				break;
			}
				
		}		
				
	}
}

	
